# -*- coding: utf-8 -*-
"""
We adapt the tutorial from pytorch. 
We use in this code the observations from pytorch and not the image
"""
from comet_ml import Experiment
import gym
import copy
from itertools import count
from functools import partial
import torch.optim as optim
import torch 
import torch.nn as nn
import tools
import model
import testing
from time import time
import numpy as np
import random
from envs import ChainEnv
from logger import tprint
import wrappers
import argparse as ap
from tqdm import tqdm
import matplotlib
from replaymemory import ReplayMemory
import gin
from agents import create_agent
from environments import create_env
from pathlib import Path
import os

import torch.nn.functional as F

@gin.configurable(blacklist=['args', 'env', 'algorithm', 'experiment'])
def run(args, env, algorithm, experiment, n_episodes, burnin_len, n_test_runs,
        device):

    #torch.set_num_threads(1)

    # ReplayMemory creation + algorithm instantiation
    memory = ReplayMemory()
    
    # -------------------------------------------------------------------------
    # Training loop
    # -------------------------------------------------------------------------
    experiment.log_parameters(vars(args))
    experiment.train()
    t_start = time()
    steps_done = 0
    info_run = {}
    if algorithm.name == 'denn':
        info_run['repulsive_points'] = []
    

    # -------------------------------------------------------------------------
    # BURN IN period: populate the replay memory with initial transitions using a random policy
    observation = env.reset()
    observation = torch.from_numpy(observation).view(1,-1).float()
    for i in range(burnin_len):
        action = env.action_space.sample()
        observation_, reward, done, _ = env.step(action)
        # Some processing
        reward = torch.tensor([reward], device=device).float()
        observation_ = torch.from_numpy(observation_).view(1,-1).float()
        action = torch.tensor([[action]]).long()
        if done:
            observation_ = None

        # Sampling the boostrapping weight
        if algorithm.name == 'bootstrapped_dqn':
            wt = tools.sample_bootstrap_weights(algorithm.n_heads).reshape(1,-1)
        else:
            wt = 1
        memory.push(observation, action, observation_, reward, wt)  # We save torch Tensors
        observation = observation_

        if done:
            observation = env.reset()

    # Finishing the trajectory so that the environment can be reset
    while not done:
        _, _, done, _ = env.step(env.action_space.sample())

    tprint('Replay memory populated with {} transitions'.format(len(memory))) 
    # ACTUAL TRAINING
    running_episode_return = 0
    consecutive_optimal = 0
    test_rewards = torch.zeros((n_episodes // args.metrics_freq + 1, n_test_runs))
    reference_net = None

    _tqdm = tqdm(range(n_episodes))
    for i_episode in _tqdm:
        _tqdm.set_description('running reward: {}'.format(running_episode_return))

        # Initialize the environment and state
        observation = env.reset()
        observation = torch.from_numpy(observation).view(1,-1).float()
        episode_undisc_return = 0
        info_episode = {}

        if algorithm.name == 'bootstrapped_dqn':
            # Choose a head
            k = np.random.randint(0, algorithm.n_heads)
            info_episode['k'] = k
            
        for t in count():
            action = algorithm.act(observation, steps_done=steps_done, **info_episode)
            observation_, reward, done, _ = env.step(action.item())
            observation_ = torch.from_numpy(observation_).view(1,-1).float()
            action = torch.tensor([[action]]).long()

            steps_done += 1
            reward = torch.tensor([reward], device=device).float()
            if done:
                observation_ = None

            # Store the transition in memory
            if algorithm.name == 'bootstrapped_dqn':
                wt = tools.sample_bootstrap_weights(algorithm.n_heads).reshape(1,-1)
            else:
                wt = 1
            memory.push(observation, action, observation_, reward, wt)  # We save torch Tensors
            observation = observation_ # Move to the next state
            episode_undisc_return += float(reward)

            # Perform one step of the optimization after each timestep
            if steps_done % algorithm.optimization_period == 0:
                if algorithm.name == 'denn':
                    info = algorithm.update(memory, reference_net, 0, env.observation_space.shape[0])
                else:
                    info = algorithm.update(memory)

                if i_episode % args.metrics_freq == 0:
                    # Logging kevery metric in the info dict 
                    for k, v in info.items(): 
                        if k[0] != '_':
                            experiment.log_metric(k, v, step=steps_done) 
                    if algorithm.name == 'denn':
                        info_run['repulsive_points'].append(info['_repulsive_points'])

            # Update the target network, copying all weights and biases in DQN
            if steps_done % algorithm.target_update_period == 0:
                if args.verbose:
                    tprint('Episode #{}, target update!'.format(i_episode))
                algorithm.update_target_net() # Only if it's a DQN type qlgo 

            if done:
                running_episode_return = running_episode_return * 0.9 + 0.1 * episode_undisc_return
                break

        if algorithm.name == 'denn':
            if i_episode % algorithm.reference_net_update_period == 0:
                if args.verbose:
                    tprint('Episode #{}, reference update')
                reference_net = copy.deepcopy(algorithm.policy_net)
                reference_net.eval()
    
        # Metrics evaluation
        # ---------------------------------------------------------------------
        if i_episode % args.metrics_freq == 0:
            experiment.log_metric("running_reward", running_episode_return, 
                    step=i_episode)
            
            for j_run in range(n_test_runs):
                test_rewards[(i_episode + 1) // args.metrics_freq, j_run] = \
                        testing.rollout(algorithm, env)
            
            # Add the tensor to the args returned by the function
            info_run['test_rewards'] = test_rewards

            if args.verbose:
                # Display some info:
                tprint('--------------')
                tprint('{:.2f}s since beginning training. Episode {}/{} ({} total steps)'.format(
                            time() - t_start, i_episode,
                            n_episodes, steps_done))
                tprint("Running reward: {}".format(
                    round(running_episode_return, 2)))

    tprint('Run complete')
    return info_run

def main():

    parser = ap.ArgumentParser()
    # RL flags
    parser.add_argument('--config', type=str, required=True, help='Path to the gin configuration fiile')
    parser.add_argument('--seed', type=int, default=0)
    # General flags
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--id', type=int)
    parser.add_argument('--video', action='store_true')
    parser.add_argument('--save_folder', type=str, default='log/mc')
    parser.add_argument('--save_freq', type=int, default=1000, help='frequency at which to save the model')
    parser.add_argument('--metrics_freq', type=int, default=10)
    parser.add_argument('--comet', action='store_true')

    args = parser.parse_args()
    gin.parse_config_file(args.config)

    # Fixing seed
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    projname = 'mc'
    experiment = Experiment(api_key="7nxUC35g9XtsxFdKcfU2wt0kI", project_name=projname, workspace="maxwab",
            disabled=not args.comet)

    # We initialize the environment and the policy network that will depend on this environment
    env = create_env()
    algo = create_agent(env)
    # Train the neural network
    print(gin.operative_config_str())

    # Run
    info_run = run(args, env, algo, experiment)

    # Saving the test rewards tensor
    filename_tensor = 'experiment_mc'
    if args.id is not None:
        filename_tensor += '_{}'.format(str(args.id))
    filename_tensor += '.pt'
    path_save_tensor = Path(args.save_folder) / filename_tensor

    path_save = Path(args.save_folder)
    if not Path.exists(path_save):
        os.makedirs(path_save)

    torch.save(info_run['test_rewards'], path_save_tensor)

if __name__ == '__main__':
    main()

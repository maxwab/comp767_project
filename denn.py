import model
import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import random
import tools
import layers
from torch.distributions.multinomial import Multinomial
import gin
import wrappers
import copy

from collections.__init__ import namedtuple
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'bsw'))

@gin.configurable
class DENN(object):
    r'''
    Modified DQN from Osband, with the repulsive loss
    '''
    name = 'denn'
    def __init__(self, policy_builder,
            discount_factor, eps_start_value, eps_end_value, eps_len_decay,
            batch_size, optimizer, crit,
            lambda_repulsive, repulsive_batch_size,
            crit_repulsive,
            disable_double_q, 
            target_update_period, optimization_period,
            reference_net_update_period,
            device='cpu'):

        self.disable_double_q = disable_double_q
        self.discount_factor = discount_factor
        # Preparing the loss function
        self.criterion = crit()
        self.lambda_repulsive = lambda_repulsive
        self.criterion_repulsive = crit_repulsive()
        self.eps_start_value = eps_start_value
        self.eps_end_value = eps_end_value
        self.eps_len_decay = eps_len_decay
        self.batch_size = batch_size
        self.repulsive_batch_size = repulsive_batch_size
        self.device = device 
        self.target_update_period = target_update_period
        self.optimization_period = optimization_period
        self.reference_net_update_period = reference_net_update_period

        # Instantation of the network
        self.policy_net = policy_builder()
        self.target_net = copy.deepcopy(self.policy_net)
        self.target_net.eval()  # No dropout, no training

        # Preparing the optimizer
        self.optimizer = optimizer(self.policy_net.parameters()) 

        # ---------------------------------------------------------------------

    def update(self, memory, reference_net, *args):
        '''
        args[0]: max_states_visited
        args[1]: n_states
        '''
        info = {}

        transitions = memory.sample(self.batch_size)
        batch = Transition(*zip(*transitions))  # Attribut state: (tensor0, tensor1, ...)

        non_final_mask = torch.tensor(
                tuple(map(lambda s: s is not None, batch.next_state)), 
                device=self.device, dtype=torch.uint8)

        state_batch = torch.cat(batch.state)  # To get one tensor of it.
        action_batch = torch.cat(batch.action)  # To get one tensor of it.
        reward_batch = torch.cat(batch.reward)  # To get one tensor of it.
        
        next_state_values = torch.zeros((state_batch.shape[0], 1),
                                        device=self.device)  # Default value: 0 because if done we want only the reward
        non_final_next_states = torch.cat([s for i, s in enumerate(batch.next_state) if s is not None])  
        if not self.disable_double_q:
            self.policy_net.eval()
            argmax_next_a_policy = self.policy_net(non_final_next_states).detach().max(1)[1].view(-1, 1)
            self.policy_net.train()
            # ^Some of the states in the batch are final states thus we do not take them into account
            next_state_values[non_final_mask] = self.target_net(non_final_next_states).detach().gather(1, argmax_next_a_policy)
            next_max_state_action_values = reward_batch.unsqueeze(1) + (
                    self.discount_factor * next_state_values)  # Compute the expected Q values

        else:
            next_state_values[non_final_mask] = self.target_net(non_final_next_states).detach().max(1)[0].unsqueeze(
                1)
            # ^Some of the states in the batch are final states thus we do not take them into account!
            next_max_state_action_values = reward_batch.unsqueeze(1) + (
                    self.discount_factor * next_state_values)  # Compute the next state Q values

        state_action_values = self.policy_net(state_batch).gather(1, action_batch)  # Compute Q(s_t, a)

        # Computation of the repulsive loss
        if reference_net is not None:

            repulsive_memory = memory.sample(self.repulsive_batch_size)
            repulsive_basis_pts = torch.cat(
                list(Transition(*zip(*repulsive_memory)).state))  # Attribut state: (tensor0, tensor1, ...)
            # repulsive_pts = sample_repulsive_noisy(repulsive_basis_pts, repulsive_std_noise)
            repulsive_pts = sample_repulsive_therm_int(repulsive_basis_pts)
            #repulsive_pts = sample_cheat(args[0], args[1])

            reference_repulsive_output = reference_net(repulsive_pts).detach()
            policy_repulsive_output = self.policy_net(repulsive_pts)
            repulsive_loss = self.criterion_repulsive(policy_repulsive_output, reference_repulsive_output)
        else:
            repulsive_loss = torch.zeros(1)

        # Actually compute the loss
        training_loss = self.criterion(state_action_values, next_max_state_action_values)
        loss = training_loss + self.lambda_repulsive * repulsive_loss

        # Optimize the model
        self.optimizer.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1)
        self.optimizer.step()

        #info['log_loss'] = np.log10(1e-20 + loss.item())
        info['log_train_loss'] = np.log10(training_loss.item())
        info['log_repulsive_loss'] = np.log10(repulsive_loss.item())

        info['loss'] = loss.item()
        info['train_loss'] = training_loss.item()
        info['repulsive_loss'] = repulsive_loss.item()

        if reference_net is not None:
            info['_repulsive_points'] = repulsive_pts 
        else:
            info['_repulsive_points'] = None

        return info

# -----------------------------------------------------------------------------
    def act(self, state, steps_done):
        return self.act_eps_greedy(state, steps_done)

    def act_greedy(self, state):
        """
        :param state:
        :return:
        """
        with torch.no_grad():
            # t.max(1) will return largest value for column of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            self.policy_net.eval()
            res = self.policy_net(state).max(1)[1]
            self.policy_net.train()
            return res.view(-1, 1)

    def act_eps_greedy(self, state, steps_done):
        """
        Only works for one state (no batches !)
        :param state:
        :return:
        """
        # Linear decay instead of exponentials
        eps_threshold = self.eps_end_value + np.maximum(0, (self.eps_start_value - self.eps_end_value) * (1 - steps_done / self.eps_len_decay))  

        sample = random.random()
        if sample > eps_threshold:
            return self.act_greedy(state)
        else: # Perform a random action
            return torch.tensor([[random.randrange(self.policy_net.output_size)]], 
                    device=self.device, 
                    dtype=torch.long)

    def update_target_net(self):
        self.target_net.load_state_dict(self.policy_net.state_dict())


def sample_repulsive_therm_int(training_pts):
    n, d = training_pts.size()
    var = Multinomial(1, torch.tensor([1., 1., 1.])) # Adds 0, -1 or 1
    noise = var.sample((n,)).max(1)[1] - 1  # Quantity to add (the argmax - 1)
    observation_space_repulsive_pts = training_pts.sum(1) + noise.float()
    return torch.cat([wrappers.encode(e, d) for e in observation_space_repulsive_pts])

def sample_cheat(max_visited, n_states):
    return wrappers.encode(max_visited+1, n_states)


import drlbx.pytorch_external_configurables

# Definition of the environment
create_env.env_name = 'chain'
create_env.env_n_states = 90
create_env.chain_wrapper = @wrappers.Thermic
Thermic.markov = False

# Definition of the agent
create_agent.agent_name = 'bootstrapped_dqn'

# Training parameters
BootstrappedDQN.dropout_rate = 0.0
BootstrappedDQN.n_heads = 10
BootstrappedDQN.n_neurons = 16
BootstrappedDQN.optimizer = @torch.optim.Adam # Note the value of the eps parameter in the BootstrappedDQN class
torch.optim.Adam.lr = 1e-3
torch.optim.Adam.eps = 1.5e-4 # Value used in the Rainbow paper OR the exploration noise paper

BootstrappedDQN.crit = @torch.nn.SmoothL1Loss
BootstrappedDQN.batch_size = 64

# Weights initialization
init_weights.type_init = 'stable'

# RL parameters
BootstrappedDQN.discount_factor = 0.95
BootstrappedDQN.eps_start_value = 1
BootstrappedDQN.eps_end_value = .001
BootstrappedDQN.eps_len_decay = 1200 # in number of steps
BootstrappedDQN.disable_double_q = False
BootstrappedDQN.device = 'cpu'
BootstrappedDQN.target_update_period = 1000 # in number of steps
BootstrappedDQN.optimization_period = 10 # In number of steps

# Replay memory parameters 
ReplayMemory.capacity = 10000

# Experiment parameters
run.n_episodes = 2000
run.burnin_len = 64
run.device = 'cpu'

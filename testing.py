import torch
from collections import Counter

def Most_Common(lst):
    r'''found on stackoverflox
    https://stackoverflow.com/questions/1518522/find-the-most-common-element-in-a-list
    '''
    data = Counter(lst)
    return data.most_common(1)[0][0]

def is_optimal(algo, env):
    # We reset the environment
    tot_r = 0
    s = env.reset()
    done = False
    # We get the action proposed by the alorithm
    # In the case of an ensemble it's the majority action
    lr = []
    while not done:
        if algo.name in ['dqn', 'denn']:
            a = int(algo.act_greedy(s))
        elif algo.name == 'bootstrapped_dqn':
            la = [algo.act_greedy(s, k=k) for k in range(algo.n_heads)]
            a = Most_Common(la)
        s, r, done, _ = env.step(a)
        lr.append(r)
        # We cumulate the reward
        tot_r += r 
    
    # If the reward is 10, test is a success
    return sum(lr[-10:]) == 10

def rollout(algo, env):

    tot_r = 0
    s = env.reset()
    done = False
    # We get the action proposed by the alorithm
    # In the case of an ensemble it's the majority action
    lr = []
    while not done:
        s = torch.from_numpy(s).view(1,-1).float()
        if algo.name in ['dqn', 'denn']:
            a = int(algo.act_greedy(s))
        elif algo.name == 'bootstrapped_dqn':
            la = [int(algo.act_greedy(s, k=k)) for k in range(algo.n_heads)]
            a = Most_Common(la)
        s, r, done, _ = env.step(a)
        # We cumulate the reward
        tot_r += r 
    return tot_r

from dqn import DQN
from bdqn import BootstrappedDQN
from denn import DENN
import gin

@gin.configurable(blacklist=['env'])
def create_agent(env, agent_name, policy_builder):
    r'''Instantiate an agent'''
    if agent_name == 'dqn':
        builder = policy_builder(env)
        return DQN(builder)
    elif agent_name == 'bootstrapped_dqn':
        builder = policy_builder(env)
        return BootstrappedDQN(builder)
    elif agent_name == 'denn':
        builder = policy_builder(env)
        return DENN(builder)
    else:
        raise NotImplementedError('Agent has not been implemented yet')

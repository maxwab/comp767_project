from envs import ChainEnv
from envs import BimodalEnv
import gym
import wrappers
import gin
from vel.rl.env.mujoco import MujocoEnv

# From MNFDQN
gym.envs.register(
    id='MountainCarLenlen-v0',
    entry_point='gym.envs.classic_control:MountainCarEnv',
    max_episode_steps=1000,      # MountainCar-v0 uses 200
    reward_threshold=-110.0,
)

@gin.configurable
def create_env(env_name, chain_wrapper=wrappers.Thermic, env_n_states=0,
        bimodal_dim=1):
    if env_name == 'chain':
        return chain_wrapper(ChainEnv(env_n_states), env_n_states)
    elif env_name == 'bimodal':
        return chain_wrapper(BimodalEnv(bimodal_dim, env_n_states), env_n_states)
    elif env_name == 'mc':
        return gym.make('MountainCarLenlen-v0')
        #return MujocoEnv('MountainCar-v0').instantiate()
    else:
        raise NotImplementedError('This environment is not supported for the moment')


import random
from time import time
import numpy as np
import torch, torch.nn as nn, torch.nn.functional as F 

from collections.__init__ import namedtuple
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'bsw'))


def comp_eps(steps_done, EPS_START, EPS_UNTIL, EPS_END):
    return EPS_END + np.maximum(0, (EPS_START - EPS_END) * (
            1 - steps_done / EPS_UNTIL))  # Linear decay instead of exponentials


def synchronize_target_net(policy_net, target_net):
    """
    Replaces the weights of the target net with the weights of the policy net.
    :param target_net:
    :param policy_net:
    :return:
    """
    target_net.load_state_dict(policy_net.state_dict())

def sample_bootstrap_weights(n_heads):
    """
    Samples a binary vector.
    :param n_heads: (int) number of heads, and also the len of the outputted vector
    :return: (ndarray, (n_heads,), binary vector
    """
    return np.random.binomial(1, 0.5, n_heads)

def compare_models(model_1, model_2):
    models_differ = 0
    for key_item_1, key_item_2 in zip(model_1.state_dict().items(), model_2.state_dict().items()):
        if torch.equal(key_item_1[1], key_item_2[1]):
            pass
        else:
            models_differ += 1
            if (key_item_1[0] == key_item_2[0]):
                print('Mismtach found at', key_item_1[0])
            else:
                raise Exception
    if models_differ == 0:
        print('Models match perfectly! :)')

import torch
import gin
import wrappers
import numpy as np
import torch
import random
import tools
import model
import argparse as ap
from dqn import DQN
import drlbx.pytorch_external_configurables
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

parser = ap.ArgumentParser()
parser.add_argument('--n', type=int, default=10, help='number of quasi priors')
parser.add_argument('--i', type=int, default=20, help='number of states')
parser.add_argument('--o', type=int, default=1, help='output dimension')
parser.add_argument('--seed', type=int, default=0, help='random seed for reproducibility')
args = parser.parse_args()

dqn_config='''
DQN.dropout_rate = 0.0
DQN.n_neurons = 16
DQN.optimizer = @torch.optim.Adam # Note the value of the eps parameter in the DQN class
torch.optim.Adam.lr = 1e-3
torch.optim.Adam.eps = 1.5e-4 # Value used in the Rainbow paper OR the exploration noise paper
DQN.crit = @torch.nn.SmoothL1Loss
DQN.batch_size = 64
init_weights.type_init = 'default'
DQN.discount_factor = 0.95
DQN.eps_start_value = 1
DQN.eps_end_value = .001
DQN.eps_len_decay = 1200 # in number of steps
DQN.disable_double_q = False
DQN.device = 'cpu'
DQN.target_update_period = 1000 # in number of steps
DQN.optimization_period = 10 # In number of steps
'''

# Setting seed for reproducibility
np.random.seed(args.seed)
torch.manual_seed(args.seed)
random.seed(args.seed)

gin.parse_config(dqn_config, skip_unknown=False)

res = []
for i in range(args.n):
    agent = DQN(args.i, args.o)
    agent.policy_net.eval()
    qvals = []
    for s in range(args.i):
        # Thermic encoding
        #s = wrappers.encode(torch.FloatTensor([s]).view(-1,1), 
        #                    args.i)
        s = wrappers.encode_ohe(torch.FloatTensor([s]).view(-1,1), 
                            args.i)
        qvals.append(agent.policy_net(s))
    qarr = torch.cat(qvals, 0).detach().numpy()
    res.append(qarr)

plt.figure()
for e in res:
    plt.plot(range(args.i), e)
plt.savefig('default_init_ohe.pdf')

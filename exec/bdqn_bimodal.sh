cd ..
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_10.gin --save_folder log/bimodal --id 10 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_20.gin --save_folder log/bimodal --id 20 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_30.gin --save_folder log/bimodal --id 30 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_40.gin --save_folder log/bimodal --id 40 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_50.gin --save_folder log/bimodal --id 50 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_60.gin --save_folder log/bimodal --id 60 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_70.gin --save_folder log/bimodal --id 70 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_80.gin --save_folder log/bimodal --id 80 --seed 9 &
wait
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 0 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 1 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 2 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 3 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 4 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 5 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 6 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 7 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 8 &
python comp_method_chain.py --config configs/bimodal/bimodal_bdqn_stable_90.gin --save_folder log/bimodal --id 90 --seed 9 &
wait

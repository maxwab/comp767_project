cd ..
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_10.gin --save_folder log/chain/dqn --id 10 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_20.gin --save_folder log/chain/dqn --id 20 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_30.gin --save_folder log/chain/dqn --id 30 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_40.gin --save_folder log/chain/dqn --id 40 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_50.gin --save_folder log/chain/dqn --id 50 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_60.gin --save_folder log/chain/dqn --id 60 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_70.gin --save_folder log/chain/dqn --id 70 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_80.gin --save_folder log/chain/dqn --id 80 --seed 9 &
wait
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 0 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 1 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 2 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 3 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 4 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 5 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 6 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 7 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 8 &
python comp_method_chain.py --config configs/chain/stable_dqn_90.gin --save_folder log/chain/dqn --id 90 --seed 9 &
wait

import torch.nn as nn
import torch.nn.functional as F
import math
import layers
import gin


@gin.configurable
class PixelPolicy(nn.Module):
    r'''
    Code from pytorch tutorial on DQN
    '''

    def __init__(self, h=84, w=84, c=1):
        super().__init__()
        self.conv1 = nn.Conv2d(n_frames, 16, kernel_size=5, stride=2)
        self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=5, stride=2)
        self.bn3 = nn.BatchNorm2d(32)

        # Number of Linear input connections depends on output of conv2d layers
        # and therefore the input image size, so compute it.
        def conv2d_size_out(size, kernel_size = 5, stride = 2):
            return (size - (kernel_size - 1) - 1) // stride  + 1
        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        linear_input_size = convw * convh * 32
        self.head = nn.Linear(linear_input_size, 2) # 448 or 512

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        return self.head(x.view(x.size(0), -1))


@gin.configurable(blacklist=['input_size', 'output_size'])
class MLP2(nn.Module):

    def __init__(self, input_size, output_size, n_neurons, dropout_rate):
        super().__init__()
        self.input_size = input_size
        self.output_size = output_size
        # Architecture construction
        self.fc1 = nn.Linear(input_size, n_neurons)
        self.fc1_dropout = nn.Dropout(p=dropout_rate)
        self.fc2 = nn.Linear(n_neurons, n_neurons)
        self.fc2_dropout = nn.Dropout(p=dropout_rate)
        self.fco = nn.Linear(n_neurons, output_size)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc1_dropout(x)
        x = F.relu(self.fc2(x))
        x = self.fc2_dropout(x)
        return self.fco(x)

def weights_init(m, gain):
    stdv = gain / math.sqrt(m.weight.size(1))
    m.weight.data.uniform_(-stdv/3, stdv/3)
    if m.bias is not None:
        m.bias.data.uniform_(-stdv/3, stdv/3)

@gin.configurable(blacklist=['env'])
def mlp2_builder(env, mujoco=False):
    def _builder():
        if mujoco:
            policy = MLP2(env.observation_space.shape[0], env.action_space.n)
        else:
            policy = MLP2(env.observation_space.n, env.action_space.n)
        policy.apply(layers.init_weights)
        return policy
    return _builder

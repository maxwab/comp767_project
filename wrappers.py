import torch
import gin


def encode(state, n_states):
    vec = torch.zeros(1, n_states)
    vec[0,:state.int().item()+1] = 1
    return vec

def encode_ohe(state, n_states):
    vec = torch.zeros((1, n_states))
    vec[0, state.int().item()] = 1
    return vec


@gin.configurable
class Thermic(object):
    def __init__(self, env, n_states, markov=False):
        self.env = env
        self.markov = markov
         
    def __getattr__(self, attr):
        return self.env.__getattribute__(attr)

    def reset(self, *args):
        x = self.env.reset(*args)
        x = torch.FloatTensor([x]).view(1, -1)
        if self.markov:
            x = encode(x, self.n_states)
            return torch.cat([x, torch.FloatTensor([self.env.len_episode]).view(1, 1)], 1)
        else:
            return encode(x, self.n_states)

    def step(self, *args):
        res = self.env.step(*args) # First el of list is observation
        x = torch.FloatTensor([res[0]]).view(1, -1)
        if self.markov:
            x = torch.cat([encode(x, self.n_states), 
                torch.FloatTensor([self.env.len_episode - self.env.iter]).view(1, 1)], 1)
        else:
            x = encode(x, self.n_states)
        return (x,) + res[1:]


@gin.configurable
class OneHot(object):
    def __init__(self, env):
        self.env = env
    def __getattr__(self, attr):
        return self.env.__getattribute__(attr)

    def reset(self, *args):
        x = self.env.reset(*args)
        z = torch.zeros((1, self.n_states))
        z[0, x] = 1
        return z

    def step(self, *args):
        res = self.env.step(*args) # First el of list is observation
        z = torch.zeros((1, self.n_states))
        z[0, res[0]] = 1
        return (z,) + res[1:]

import os
import tools
import gym
from gym import spaces
import numpy as np
import torch
import itertools

from matplotlib import colors as mcol, cm as cm, pyplot as plt

class BimodalEnv(gym.Env):
    r'''
    Multi dimensional environment.
    '''

    metadata = {'render.modes': ['human']}
    REWARD_SUBOPT = 0.2
    REWARD_OPT = 1.0

    def __init__(self, dim, n_states):
        r'''
        dim: (int) dimension of the problem
        n_states: (int) Number of states PER DIMENSION
        '''

        self.dim = dim
        self.n_states = n_states
        self.observation_space = spaces.Discrete(self.n_states**dim)
        self.action_space = spaces.Discrete(2**dim) # Can move either left or right

        movs_along_dim = np.array([-1,1])
        self.actions = [np.asarray(e) for e in itertools.product(movs_along_dim, repeat=dim)]

        # Initialization of the reward function parameters
        self.mu_subopt = .2
        self.mu_opt = .8
        self.len_episode = n_states + 8
        self.has_been_reset = False

    def step(self, action):
        """
        Computes the discrete step using the _action_id_ (id of the action to use) and the _state_
        (continuous value of the current state).
        :param mdp: Dict, contains the parameters of the environments
        :param state: Float, Continuous state
        :param action_id: Int, Id of action to take
        :return:
        """
        assert self.has_been_reset, 'Error: environment has not been reset yet'
        self.iter += 1

        # First we generate the next state using a function to mimic the model based setting
        action_value = self.actions[action]
        self.state = self.transition_fn(self.state, action_value)

        # Then we generate the reward
        reward = float(self.reward_fn(None, None, self.state))
        done = self.iter == self.len_episode
        return self.state, reward, done, {}  # There is no terminal state

    def reward_fn(self, state, action, next_state):
        """
        Output is naturally clipped.
        :param state: (Int ndarray)
        :param action: (Int ndarray)
        :param next_state: (Int ndarray)
        :return: (Float) Reward value for this next state
        """
        if self.dim != 1:
            raise NotImplementedError('Only dim 1 implemented right now')
        else:
            float_state = float(next_state)/(self.n_states - 1)
            '''
            # Linear
            if float_state <= .2:
                return self.REWARD_SUBOPT*(1 - 5*float_state)
            elif float_state >= .8:
                return self.REWARD_OPT*(5*float_state - 4)
            else:
                return 0
            '''
            # Using the wu kernel
            if float_state <= .2:
                return self.REWARD_SUBOPT*wu_kernel(0, float_state, .2)
            elif float_state >= .8:
                return self.REWARD_OPT*wu_kernel(1, float_state, .2)
            else:
                return 0
            
        return reward

    def transition_fn(self, state, action_value):
        """
        States are integers between 0 and max_state - 1
        Outputted is clipped.
        """
        next_state = state + action_value
        next_state = np.clip(next_state, 0, self.n_states - 1)
        return next_state

    def reset(self):
        self.has_been_reset = True
        self.iter = 0
        self.state = np.zeros((1,self.dim))
        return self.state

    def render(self, mode='human', close=False):
        pass


def wu_kernel(x, y, sigma):
    rho = np.linalg.norm(x - y)/sigma
    return np.maximum(0,1-rho)**4 * (1 + 4*rho + 3 * rho**2 + .75*rho**3)

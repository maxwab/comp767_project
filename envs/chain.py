import gym
from gym import spaces
import numpy as np
import torch

# This file describes the chain experiment that is used in:
# Osband, Ian, Charles Blundell, Alexander Pritzel, and Benjamin Van Roy 2016Deep Exploration via Bootstrapped DQN. In
# Advances in Neural Information Processing Systems 29: Annual Conference on Neural Information Processing Systems 2016,
# December 5-10, 2016, Barcelona, Spain Pp. 4026–4034.
# http://papers.nips.cc/paper/6501-deep-exploration-via-bootstrapped-dqn.

class ChainEnv(gym.Env):

    metadata = {'render.modes': ['human']}
    ACTIONS = [-1, 1]
    BIG_REWARD = 1
    SMALL_REWARD = 1e-3

    def __init__(self, n_states):
        self.n_states = n_states
        self.observation_space = spaces.Discrete(self.n_states)
        self.action_space = spaces.Discrete(2)
        self.len_episode = self.n_states + 9

    def step(self, action):
        """
        Computes the discrete step using the _action_id_ (id of the action to use) and the _state_
        (continuous value of the current state).
        :return:
        """

        self.iter += 1
        action_value = self.ACTIONS[action]

        reward = self.reward_fn(self.state, action_value, None) # Reward
        self.state = self.transition_fn(self.state, action_value) # Next state
        done = self.iter == self.len_episode # Done

        return self.state, reward, done, {}  # There is no terminal state

    def reward_fn(self, state, action_value, next_state):
        if (state == self.n_states - 1) and (action_value == 1.0):
            reward = self.BIG_REWARD
        elif (state == 0 ) and (action_value == -1.0):
            reward = self.SMALL_REWARD
        else:
            reward = 0

        return reward

    def transition_fn(self, state, action_value):
        next_state = state + action_value
        next_state = np.clip(next_state, 0, self.n_states - 1)
        return next_state

    def reset(self):
        self.state = 0
        self.iter = 0
        return self.state

    def render(self, mode='human', close=False):
        pass

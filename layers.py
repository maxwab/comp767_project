import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import gin
from torch.nn.modules.loss import _Loss

@gin.configurable
def init_weights(m, type_init='default', normal=False, additional_gain_variance=1):
    r'''Weights initialization used in the MNF exploratin paper'''
    if type_init == 'default':
        pass
    elif type_init == 'stable':
        init_weights_stable(m, additional_gain_variance)
    elif type_init == 'he':
        init_weights_he(m, normal, additional_gain_variance)
    elif type_init == 'xavier':
        init_weights_xavier(m, normal, additional_gain_variance)
        

def init_weights_xavier(m, normal=False, agv=1):
    if isinstance(m, nn.Linear):
        if normal:
            torch.nn.init.xavier_normal_(m.weight, gain=math.sqrt(agv)*torch.nn.init.calculate_gain('relu'))
        else: # Uniform
            torch.nn.init.xavier_uniform_(m.weight, gain=math.sqrt(agv)*torch.nn.init.calculate_gain('relu'))
        m.bias.data.zero_()

def init_weights_stable(m, agv = 1):
    if isinstance(m, nn.Linear):
        n = m.in_features + m.out_features
        stdv = math.sqrt(4*agv / n)
        m.weight.data.normal_(0, stdv)
        m.bias.data.normal_(0, stdv)
        #m.bias.data.zero_()

def init_weights_he(m, normal=False, agv=1):
    if isinstance(m, nn.Linear):
        if normal:
            torch.nn.init.kaiming_normal_(m.weight, a=0)
            m.weight.data *= math.sqrt(agv)
        else: # Uniform
            torch.nn.init.kaiming_uniform_(m.weight, a=0)
            m.weight.data *= math.sqrt(agv)
        m.bias.data.zero_()


# Defining the repulsive losses
@gin.configurable
class CustomCrossEntropyLossContinuous(nn.CrossEntropyLoss):
    def __init__(self, bandwidth, *args):
        self.bandwidth = bandwidth
        super().__init__(*args)

    def forward(self, input_logits, target_logits):
        assert len(input_logits.size()) == 2, 'wrong size for input logits'
        assert len(target_logits.size()) == 2, 'wrong size for target logits'

        a = F.softmax(target_logits, 1)
        a_log = F.log_softmax(target_logits, 1)
        b = F.log_softmax(input_logits, 1)

        xent = - (a * b).sum(1)
        ent = - (a * a_log).sum(1)  # Entropy of targets
        rbf_xent = torch.exp(-((xent - ent) ** 2) / (2 * self.bandwidth ** 2))

        return torch.mean(rbf_xent)

@gin.configurable
class RegressionRepulsiveLoss(nn.MSELoss):
    def __init__(self, bandwidth, *args):
        self.bandwidth = bandwidth
        super().__init__(*args)

    def forward(self, net_preds, reference_preds):
        return torch.mean(torch.exp(-(1/(2*self.bandwidth**2))*torch.norm(net_preds - reference_preds, dim=1,keepdim=True)**2))

@gin.configurable
class JensenShannonDistance(_Loss):
    def __init__(self, *args):
        super().__init__(*args)
    def forward(self, a, b):  
        '''
        a: logit
        b: logit
        '''
        kl = torch.nn.KLDivLoss(reduction='none')
        ap = torch.log_softmax(a, 1)
        bp = torch.log_softmax(b, 1)
        m = .5 * (ap.exp() + bp.exp())
        # We had a max (0, .) to avoid instabilities leading the distance to be
        # negative
        dist = torch.max(torch.zeros(1), 0.5 * kl(torch.log(m),F.softmax(a, 1)).sum(1) + 0.5 * kl(torch.log(m),F.softmax(b, 1)).sum(1))
        return -torch.log(math.exp(-10)*torch.ones_like(dist) + dist).mean()


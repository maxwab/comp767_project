import model
import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import random
import tools
import layers
import gin
import copy

from collections.__init__ import namedtuple
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'bsw'))

@gin.configurable
class BootstrappedDQN(object):
    r'''
    Bootstrapped DQN from Osband
    '''
    
    name = 'bootstrapped_dqn'
    def __init__(self, policy_builder, n_heads,
            discount_factor, eps_start_value, eps_end_value, eps_len_decay,
            batch_size, optimizer, crit,
            disable_double_q, 
            target_update_period, optimization_period,
	    device=torch.device('cpu')):

        self.disable_double_q = disable_double_q
        self.discount_factor = discount_factor
        # Preparing the loss function
        self.criterion = crit()
        self.eps_start_value = eps_start_value
        self.eps_end_value = eps_end_value
        self.eps_len_decay = eps_len_decay
        self.batch_size = batch_size
        self.n_heads = n_heads
        self.device = torch.device(device)
        self.target_update_period = target_update_period
        self.optimization_period = optimization_period

        self.policy_nets = [policy_builder().train().to(self.device) for _ in range(self.n_heads)]
        self.optimizers = [optimizer(e.parameters()) for e in self.policy_nets]
        self.target_nets = [copy.deepcopy(e).eval().to(self.device) for e in self.policy_nets]

# -----------------------------------------------------------------------------
    def set_head(self, head):
        self.head = head

    def update(self, memory):
        info = {}
        losses = []
        for k in range(self.n_heads):
            self.policy_nets[k].train()
        transitions = memory.sample(self.batch_size)
        batch = Transition(*zip(*transitions))  # Attribut state: (tensor0, tensor1, ...)

        bsw_batch = np.concatenate(batch.bsw)
        non_final_mask = torch.tensor(
                tuple(map(lambda s: s is not None, batch.next_state)), 
                device=self.device, dtype=torch.uint8)


        for k in range(self.n_heads):
            idx_head = list(np.where(bsw_batch[:, k] == 1)[0])
            state_batch = torch.cat(batch.state)[idx_head].to(self.device)  # To get one tensor of it.
            action_batch = torch.cat(batch.action)[idx_head].to(self.device)  # To get one tensor of it.
            reward_batch = torch.cat(batch.reward)[idx_head].to(self.device)  # To get one tensor of it.

            non_final_mask_head = non_final_mask[idx_head].to(self.device)

            if len(idx_head) > 0:  # if the batch is not empty
                next_state_values = torch.zeros((len(idx_head), 1),
                                                device=self.device)  # Default value: 0 because if done we want only the reward
                non_final_next_states = torch.cat([s for i, s in enumerate(batch.next_state)
                                                   if (s is not None and i in idx_head)]).to(self.device)  # DOIT ETRE DE TAILLE NOMBRE DELEMENT OU WT = 1
                if not self.disable_double_q:
                    self.policy_nets[k].eval()
                    argmax_next_a_policy = self.policy_nets[k](non_final_next_states).detach().max(1)[1].view(-1, 1)
                    self.policy_nets[k].train()
                    # ^Some of the states in the batch are final states thus we do not take them into account
                    next_state_values[non_final_mask_head] = self.target_nets[k](non_final_next_states).detach().gather(1, argmax_next_a_policy)
                    next_max_state_action_values = reward_batch.unsqueeze(1) + (
                            self.discount_factor * next_state_values)  # Compute the expected Q values

                else:
                    next_state_values[non_final_mask_head] = self.target_nets[k](non_final_next_states).detach().max(1)[0].unsqueeze(1)
                    # ^Some of the states in the batch are final states thus we do not take them into account!
                    next_max_state_action_values = reward_batch.unsqueeze(1) + (
                            self.discount_factor * next_state_values)  # Compute the next state Q values

                state_action_values = self.policy_nets[k](state_batch).gather(1, action_batch)  # Compute Q(s_t, a)
                loss = self.criterion(state_action_values, next_max_state_action_values)
                
                # Optimize the model
                self.optimizers[k].zero_grad()
                loss.backward()
                for param in self.policy_nets[k].parameters():
                    param.grad.data.clamp_(-1, 1)
                self.optimizers[k].step()
                losses.append(loss.item())
        info['log_loss'] = np.log10(loss.item())
        return info

# -----------------------------------------------------------------------------
    def act(self, state, k=None, **kwargs):
        return self.act_greedy(state, k=k)

    def act_greedy(self, state, k=None, **kwargs):
        """
        Only works for one state (no batches !)
        :param state:
        :return:
        """
        #self.policy_net.eval()
        with torch.no_grad():
            # t.max(1) will return largest value for column of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            res = self.policy_nets[k](state.to(self.device)).detach().max(1)[1]
            return res.view(1, 1)

    def update_target_net(self):
        # First we ensure that the weights of the target net and the policy net are the same
        for k in range(self.n_heads):
            self.target_nets[k].load_state_dict(self.policy_nets[k].state_dict())



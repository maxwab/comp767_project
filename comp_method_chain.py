# -*- coding: utf-8 -*-
"""
We adapt the tutorial from pytorch. 
We use in this code the observations from pytorch and not the image
"""
from comet_ml import Experiment
from pathlib import Path
import os
import tools
import numpy as np
from envs import ChainEnv
from logger import tprint
import argparse as ap
import gin
from agents import create_agent
from environments import create_env
from train_chain import run

def main():
    parser = ap.ArgumentParser()
    # RL flags
    parser.add_argument('--config', type=str, required=True, help='Path to the gin configuration fiile')
    parser.add_argument('--seed', type=int, default=0)
    # General flags
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--id', type=int)
    parser.add_argument('--save_folder', type=str, default='log/chain')
    parser.add_argument('--save_freq', type=int, default=100, help='frequency at which to save the model')
    parser.add_argument('--metrics_freq', type=int, default=1)
    parser.add_argument('--comet', action='store_true')

    args = parser.parse_args()
    gin.parse_config_file(args.config)

    projname = 'chain_instability_dqn'
    experiment = Experiment(api_key="7nxUC35g9XtsxFdKcfU2wt0kI", project_name=projname, workspace="maxwab",
            disabled=not args.comet)

    # We initialize the environment and the policy network that will depend on this environment
    env = create_env()
    algo = create_agent(env)
    # Train the neural network
    print(gin.operative_config_str())
    info_run = run(args, env, algo, experiment)

    # Now we retrieve the list of highest states visited and store it
    # In binary because it's economical and it's a list of float 
    path_save = Path(args.save_folder)
    if not Path.exists(path_save):
        os.makedirs(path_save)
    filename = 'ns:{}_seed:{}.npy'.format(args.id, args.seed)
    try:
        with open(path_save / filename, 'wb') as fd:
            np.save(fd, np.asarray(info_run['episode_success']))
    except:
        tprint('Error, could not save the file')

if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
"""
We adapt the tutorial from pytorch. 
We use in this code the observations from pytorch and not the image
"""
from comet_ml import Experiment
import os
import gym
import copy
from itertools import count
from functools import partial
import torch.optim as optim
import torch 
import torch.nn as nn
import tools
import model
import testing
from time import time
import numpy as np
import random
from envs import ChainEnv
from logger import tprint
import wrappers
import argparse as ap
from tqdm import tqdm
import matplotlib
from replaymemory import ReplayMemory
import gin
from agents import create_agent
from environments import create_env
from pathlib import Path

import torch.nn.functional as F

@gin.configurable(blacklist=['args', 'env', 'algorithm', 'experiment'])
def run(args, env, algorithm, experiment, n_episodes, burnin_len, device):

    torch.set_num_threads(1)
    # Instantiation of the environment
    init_obs = env.reset()

    # ReplayMemory creation + algorithm instantiation
    memory = ReplayMemory()
    
    # -------------------------------------------------------------------------
    # Training loop
    # -------------------------------------------------------------------------
    experiment.log_parameters(vars(args))
    experiment.train()
    t_start = time()
    steps_done = 0
    info_run = {}
    if algorithm.name == 'denn':
        info_run['repulsive_points'] = []
    

    # -------------------------------------------------------------------------
    # BURN IN period: populate the replay memory with initial transitions using a random policy
    observation = env.reset()
    highest_state_visited = torch.sum(observation[0,:env.n_states]).float() - 1 # from 0 to n-1
    lhsv =  []
    qarrs = []
    qarrs_target = []
    qarrs_repulsive = []
    for i in range(burnin_len):
        action = torch.tensor([[random.randrange(2)]], device=device, dtype=torch.long)  # Choose the action
        observation_, reward, done, _ = env.step(action.item())
        highest_state_visited = max(highest_state_visited, torch.sum(observation_[0,:env.n_states]).float() - 1)
        # Some processing
        reward = torch.tensor([reward], device=device).float()

        if done:
            observation_ = None

        # Sampling the boostrapping weight
        if algorithm.name == 'bootstrapped_dqn':
            wt = tools.sample_bootstrap_weights(algorithm.n_heads).reshape(1,-1)
        else:
            wt = 1
        memory.push(observation, action, observation_, reward, wt)  # We save torch Tensors
        observation = observation_

        if done:
            observation = env.reset()

    tprint('Replay memory populated with {} transitions'.format(len(memory))) 
    # ACTUAL TRAINING
    running_episode_return = 0
    consecutive_optimal = 0
    reference_net = None

    _tqdm = tqdm(range(n_episodes))
    for i_episode in _tqdm:

        # Initialize the environment and state
        observation = env.reset()
        episode_undisc_return = 0

        if algorithm.name == 'bootstrapped_dqn':
            # Choose a head
            k = np.random.randint(0, algorithm.n_heads)
            algorithm.set_head(k)
            
        for t in count():
            action = algorithm.act(observation, steps_done)
            observation_, reward, done, _ = env.step(action.item())
            steps_done += 1
            highest_state_visited = max(highest_state_visited, torch.sum(observation_[0,:env.n_states]).float() - 1)
            reward = torch.tensor([reward], device=device).float()
            if done:
                observation_ = None

            # Store the transition in memory
            if algorithm.name == 'bootstrapped_dqn':
                wt = tools.sample_bootstrap_weights(algorithm.n_heads).reshape(1,-1)
            else:
                wt = 1
            memory.push(observation, action, observation_, reward, wt)  # We save torch Tensors
            observation = observation_ # Move to the next state

            episode_undisc_return += float(reward)

            # Perform one step of the optimization after each timestep
            if steps_done % algorithm.optimization_period == 0:
                if algorithm.name == 'denn':
                    info = algorithm.update(memory, reference_net, highest_state_visited, env.observation_space.n)
                else:
                    info = algorithm.update(memory)

                # Logging every metric in the info dict 
                for k, v in info.items(): 
                    if k[0] != '_':
                        experiment.log_metric(k, v, step=steps_done) 
                if algorithm.name == 'denn':
                    info_run['repulsive_points'].append(info['_repulsive_points'])

            # Update the target network, copying all weights and biases in DQN
            if steps_done % algorithm.target_update_period == 0:
                if args.verbose:
                    tprint('Episode #{}, target update!'.format(i_episode))
                algorithm.update_target_net() # Only if it's a DQN type qlgo 

            if done:
                running_episode_return = running_episode_return * 0.9 + 0.1 * episode_undisc_return
                # We also evaluate the value of the first state
                if testing.is_optimal(algorithm, env):
                    consecutive_optimal += 1
                else:
                    consecutive_optimal = 0
                _tqdm.set_description('{} consecutive optimal policies ({} highest)'.format(consecutive_optimal, highest_state_visited))
                break


        lhsv.append(highest_state_visited) # We record the highest state visited at the end of each episode

        if args.video:
            if algorithm.name == 'bootstrapped_dqn':
                # Figuring out the Q values
                all_qarr= []
                for net in algorithm.policy_nets:
                    qvals = []
                    net.eval()
                    for s in range(env.observation_space.n):
                        s = wrappers.encode(torch.FloatTensor([s]).view(-1,1), 
                                env.observation_space.n)
                        qvals.append(net(s))
                    net.train()
                    all_qarr.append(torch.cat(qvals, 0).detach().numpy())
                all_qarrs = np.stack(all_qarr, -1) 
                qarrs.append(all_qarrs)
                info_run['q_values'] = qarrs
            else:
                # Figuring out the Q values
                qvals = []
                algorithm.policy_net.eval()
                for s in range(env.observation_space.n):
                    s = wrappers.encode(torch.FloatTensor([s]).view(-1,1), 
                            env.observation_space.n)
                    qvals.append(algorithm.policy_net(s))
                algorithm.policy_net.train()
                qarr = torch.cat(qvals, 0).detach().numpy()
                qarrs.append(qarr)
                info_run['q_values'] = qarrs

                if algorithm.name == 'denn':
                    if reference_net is not None:
                        # Figuring out the Q values
                        qvals = []
                        for s in range(env.observation_space.n):
                            s = wrappers.encode(torch.FloatTensor([s]).view(-1,1), 
                                    env.observation_space.n)
                            qvals.append(reference_net(s))
                        qarr = torch.cat(qvals, 0).detach().numpy()
                        qarrs_repulsive.append(qarr)
                        info_run['q_values_repulsive'] = qarrs_repulsive
                    else:
                        qarrs_repulsive.append(torch.zeros(env.observation_space.n, 2))
                        info_run['q_values_repulsive'] = qarrs_repulsive


        if algorithm.name == 'denn':
            if i_episode % algorithm.reference_net_update_period == 0:
                if args.verbose:
                    tprint('Episode #{}, reference update')
                reference_net = copy.deepcopy(algorithm.policy_net)
                reference_net.eval()

        '''
        # Debugging: show policy
        __ = torch.cat([wrappers.encode(torch.FloatTensor([i]).view(-1,1), env.observation_space.n) for i in range(env.observation_space.n)], 0)
        algorithm.policy_net.eval()
        qvals = algorithm.policy_net(__)
        qvals_ref = reference_net(__)
        _x, _y = F.softmax(qvals,1), F.softmax(qvals_ref, 1)
        algorithm.policy_net.train()
        import layers
        _d = layers.JensenShannonDistance()
        import pdb; pdb.set_trace()
        '''
    
        # Metrics evaluation
        # ---------------------------------------------------------------------
        if i_episode % args.metrics_freq == 0:
            curr_eps = tools.comp_eps(steps_done, algorithm.eps_start_value, 
                    algorithm.eps_len_decay, algorithm.eps_end_value)
            # experiment.log_metric("last_episode_duration", t+1, step=steps_done)
            experiment.log_metric("running_reward", running_episode_return, step=i_episode)
            experiment.log_metric("highest_state_visited", highest_state_visited, step=i_episode)
            
            if args.verbose:
                # Display some info:
                tprint('--------------')
                tprint('{:.2f}s since beginning training. Episode {}/{} ({} total steps)'.format(time() - t_start, i_episode,
                    n_episodes, steps_done))
                tprint('Last duration: {}, epsilon = {:.2f}'.format(t + 1, curr_eps))
                tprint("Running reward: {}".format(round(running_episode_return, 2)))
                tprint('Highest state visited: {}'.format(highest_state_visited))
    
        info_run['episode_success'] = i_episode
        info_run['list_highest_states'] = lhsv

        # If we have it 100 consecutive optimal returns we stop the training
        if consecutive_optimal == 100:
            tprint('Episode #{}, we have hit 100 consecutive optimal returns'.format(i_episode))
            experiment.log_metric('success_episode', i_episode)
            return info_run

    tprint('Run complete')
    return info_run

def main():

    parser = ap.ArgumentParser()
    # RL flags
    parser.add_argument('--config', type=str, required=True, help='Path to the gin configuration fiile')
    parser.add_argument('--seed', type=int, default=0)
    # General flags
    parser.add_argument('--verbose', action='store_true')
    parser.add_argument('--id', type=int)
    parser.add_argument('--video', action='store_true')
    parser.add_argument('--save', action='store_true')
    parser.add_argument('--save_folder', type=str, default='log/chain')
    parser.add_argument('--save_freq', type=int, default=100, help='frequency at which to save the model')
    parser.add_argument('--metrics_freq', type=int, default=1)
    parser.add_argument('--comet', action='store_true')

    args = parser.parse_args()
    gin.parse_config_file(args.config)

    # Fixing seed
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    projname = 'debug'
    experiment = Experiment(api_key="7nxUC35g9XtsxFdKcfU2wt0kI", project_name=projname, workspace="maxwab",
            disabled=not args.comet)

    # We initialize the environment and the policy network that will depend on this environment
    env = create_env()
    algo = create_agent(env)
    # Train the neural network
    print(gin.operative_config_str())

    info_run = run(args, env, algo, experiment)

    path_save = Path(args.save_folder)
    if args.save:
        if not Path.exists(path_save):
            os.makedirs(path_save)
        
        filename = 'hsv_{}.npy'.format(args.seed)
        with open(path_save / filename, 'wb') as fd:
            np.save(fd, info_run['list_highest_states'])

        filename = 'episode_success_{}.npy'.format(args.seed)
        with open(path_save / filename, 'wb') as fd:
            np.save(fd, info_run['episode_success'])
if __name__ == '__main__':
    main()

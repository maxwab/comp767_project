# COMP767 - Final project

This repository contains the code used for project titled "Randomized value functions and diversity constraints for exploration in reinforcement learning".

# Files
- envs/ : contains the environments used. Deepsea code was taken from Osband's repository
- logger.py : contains functions for logging during training.
- replaymemory.py : contains the basic replay memory. Taken from the pytorch tutorial on DQN.
- notebooks/ : contains notebooks where some figures of the report were created
- comp_method_chain.py : used to evaluate the performance of the algorithms on the chain environment
- tools.py : useful functions
- wrappers.py : wrappers for the chain environment to modify the encoding of the states
- environments.py : functions to instantiate the environment, to be used with gin config
- agents.py : same, for agents
- model.py : contains the nets used for the experiments
- layers.py : where custom layers and losses are defined
- bdqn.py : contains the Bootstrapped DQN algorithm
- dqn.py : contains the DQN algorithm
- denn.py : contains the denn algorithm
- testing.py : contains the functions used to evaluate the performance of the policies at test time. 
- video_instabiliti_dqn.py : used to create video illustrating the instability of the DQN during training if the hyperparameters are not carefully chosen
- train_chain.py : contains the `run` function used to run experiments on the chain environment
- train_mc.py : same, for the Mountain Car environment
- visualisation/ : contains notebooks to generate figures for the report
- quasi_priors.py : creates figures of the shapes of initialized, untrained neural networks
- exec/ : contains the .sh files to run the experiments


# Configuration files
We use the gin-config repository (https://github.com/google/gin-config) to manage our experiments.
The different configuration we use can be found in configs/

# Used repositories
The base code for the DQN and bootstrap DQN is based on the default DQN implementation from PyTorch tutorial (https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html) as well as Osband's colab presenting randomized prior functions (https://colab.research.google.com/drive/1hOZeHjSdoel_-UoeLfg1aRC4uV_6gX1g).
Some design choices were inspired by the code used for the paper "Randomized value functions via multiplicative normalizing flows", and parts of the experiments were based on the paper (and the code).
Finally, first experiments on Atari (not presented in the report!) are done using the Vel library (https://github.com/MillionIntegrals/vel).

# Run experiments
To run again the experiments, launch any of the .sh files in exec/:
- bdqn_chain.sh: evaluates the performance of Bootstrapped DQN on the chain environment for several chain length.
- dqn_chain.sh: same, for the DQN algorithm
- bdqn_bimodal.sh: same, but on an environment similar to chain, but more stable
- denn_chain.sh: shows the unstability of the denn algorithm
- launch_mc_dqn.sh: evaluates the performance of the DQN algorithm on Mountain Car
- launch_mc_bdqn.sh: same, for the Bootstrapped DQN algorithm





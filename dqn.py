import model
import torch
import torch.optim as optim
import torch.nn as nn
import numpy as np
import random
import tools
import layers
import gin
import copy

from collections.__init__ import namedtuple
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'bsw'))

@gin.configurable
class DQN(object):
    r'''
    DQN from Osband's implementation
    '''

    name = 'dqn'
    def __init__(self, policy_builder,
            discount_factor, eps_start_value, eps_end_value, eps_len_decay,
            batch_size, optimizer, crit,
            disable_double_q, 
            target_update_period, optimization_period, device=torch.device('cpu')):

        self.disable_double_q = disable_double_q
        self.discount_factor = discount_factor
        # Preparing the loss function
        self.criterion = crit()
        self.eps_start_value = eps_start_value
        self.eps_end_value = eps_end_value
        self.eps_len_decay = eps_len_decay
        self.batch_size = batch_size
        self.target_update_period = target_update_period
        self.optimization_period = optimization_period
        self.device = torch.device(device)

        # Instantation of the network
        self.policy_net = policy_builder()
        self.target_net = copy.deepcopy(self.policy_net)
        self.target_net.eval()  # No dropout, no training

        # Preparing the optimizer
        self.optimizer = optimizer(self.policy_net.parameters()) 

    def update(self, memory):
        info = {} # Stores the update logs
        transitions = memory.sample(self.batch_size)
        batch = Transition(*zip(*transitions))  # Attribut state: (tensor0, tensor1, ...)

        non_final_mask = torch.tensor(
                tuple(map(lambda s: s is not None, batch.next_state)), 
                device=self.device, dtype=torch.uint8)

        state_batch = torch.cat(batch.state)  # To get one tensor of it.
        action_batch = torch.cat(batch.action)  # To get one tensor of it.
        reward_batch = torch.cat(batch.reward)  # To get one tensor of it.
        
        next_state_values = torch.zeros((state_batch.shape[0], 1),
                                        device=self.device)  # Default value: 0 because if done we want only the reward
        non_final_next_states = torch.cat([s for i, s in enumerate(batch.next_state) if s is not None])  
        if not self.disable_double_q:
            self.policy_net.eval()
            argmax_next_a_policy = self.policy_net(non_final_next_states).detach().max(1)[1].view(-1, 1)
            self.policy_net.train()
            # ^Some of the states in the batch are final states thus we do not take them into account
            next_state_values[non_final_mask] = self.target_net(non_final_next_states).detach().gather(1, argmax_next_a_policy)
            next_max_state_action_values = reward_batch.unsqueeze(1) + (
                    self.discount_factor * next_state_values)  # Compute the expected Q values

        else:
            next_state_values[non_final_mask] = self.target_net(non_final_next_states).detach().max(1)[0].unsqueeze(
                1)
            # ^Some of the states in the batch are final states thus we do not take them into account!
            next_max_state_action_values = reward_batch.unsqueeze(1) + (
                    self.discount_factor * next_state_values)  # Compute the next state Q values

        state_action_values = self.policy_net(state_batch).gather(1, action_batch)  # Compute Q(s_t, a)

        # Actually compute the loss
        loss = self.criterion(state_action_values, next_max_state_action_values)

        # Optimize the model
        self.optimizer.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1)
        self.optimizer.step()
        info['log_loss'] = np.log10(loss.item())

        return info

# -----------------------------------------------------------------------------
    def act(self, state, steps_done):
        return self.act_eps_greedy(state, steps_done)

    def act_greedy(self, state):
        """
        :param state:
        :return:
        """
        with torch.no_grad():
            # t.max(1) will return largest value for column of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            self.policy_net.eval()
            res = self.policy_net(state).max(1)[1]
            self.policy_net.train()
            return res.view(-1, 1)

    def act_eps_greedy(self, state, steps_done):
        """
        Only works for one state (no batches !)
        :param state:
        :return:
        """
        # Linear decay instead of exponentials
        eps_threshold = self.eps_end_value + np.maximum(0, (self.eps_start_value - self.eps_end_value) * (1 - steps_done / self.eps_len_decay))  

        sample = random.random()
        if sample > eps_threshold:
            return self.act_greedy(state)
        else: # Perform a random action
            return torch.tensor([[random.randrange(self.policy_net.output_size)]], 
                    device=self.device, 
                    dtype=torch.long)

    def update_target_net(self):
        self.target_net.load_state_dict(self.policy_net.state_dict())

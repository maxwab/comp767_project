from datetime import datetime
def tprint(message):
    assert type(message) == str
    timeinfo = datetime.now().strftime('%Y-%m-%d %H-%M-%S')
    print(timeinfo + ' -- ' + message)
